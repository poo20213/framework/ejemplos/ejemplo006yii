<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peliculas".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $descripcion
 * @property int|null $duracion
 * @property string|null $fecha_estreno
 * @property string|null $categoria
 * @property string|null $portada
 * @property string|null $director
 * @property int|null $destacada
 * @property int|null $peliculaSemana
 */
class Peliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duracion', 'destacada', 'peliculaSemana'], 'integer'],
            [['fecha_estreno'], 'safe'],
            [['titulo', 'descripcion', 'categoria', 'portada', 'director'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'duracion' => 'Duracion',
            'fecha_estreno' => 'Fecha Estreno',
            'categoria' => 'Categoria',
            'portada' => 'Portada',
            'director' => 'Director',
            'destacada' => 'Destacada',
            'peliculaSemana' => 'Pelicula Semana',
        ];
    }
}
