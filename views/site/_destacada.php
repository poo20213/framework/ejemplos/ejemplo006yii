<?php

use yii\helpers\Html;
?>
<div class="row">
    <div class="col-lg-4">
    <?php
echo Html::img("@web/imgs/$model->portada", [
    "class" => "card-img-top"
    ]);
?>
</div>
    <div class="col-lg-8">
        <h2><?= $model->titulo ?><br></h2>
        Duracion:<br>
        <?= $model->duracion ?> minutos<br>
        Director:<br>
        <?= $model->director ?><br>
        Categoria:
        <?= $model->destacada ?><br><br>
        <?=
            Html::a('Ver mas ...', 
                ['site/verpelicula', 'id' => $model->id], 
                ['class' => '']) 
        ?><br>
    </div>
</div>
