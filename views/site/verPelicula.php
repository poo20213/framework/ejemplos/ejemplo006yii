<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    'model' => $pelicula,
    'attributes' => [
        'id',
        'titulo',
        'descripcion',
        'duracion',
        'fecha_estreno',
        'categoria',
        'director',
        'destacada',
        'peliculaSemana',
        [
            'label'=>'foto', // el nombre del campo de la base de datos
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->portada;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=> 'width:205px']); 
            }
        ],
    ]
]);

