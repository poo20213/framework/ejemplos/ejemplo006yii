<?php

use yii\grid\GridView;
use yii\helpers\Html;
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'titulo',
        'fecha_estreno',
        [
            'label'=>'foto', // el nombre del campo de la base de datos
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->portada;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=> 'width:200px']); 
            }
        ],
    ]
]);
        

