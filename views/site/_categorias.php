<?php
    use yii\helpers\Html;
    
?>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Categoria: <?= $model->categoria?></li>
    <li class="list-group-item">
                <?= Html::a('Ver mas ...', 
                        ['site/ver', 'categoria' => $model->categoria], 
                        ['class' => 'btn btn-primary']) ?>
    </li>
  </ul>

