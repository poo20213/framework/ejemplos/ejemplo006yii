<?php

use yii\grid\GridView;
use yii\helpers\Html;


echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'titulo',
        [
            'label'=>'Categoria', // el nombre del campo de la base de datos
            'format'=>'raw',
            'value' => function($data){
                return Html::a(
                        $data->categoria,
                        ['site/vergrid',"id"=>$data->id],
                        ["class"=> 'btn btn-info']); 
            }
        ],
        'fecha_estreno',
        [
            'label'=>'Portada',
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->portada;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=> 'width:205px']); 
            }
        ],
    ],
]);