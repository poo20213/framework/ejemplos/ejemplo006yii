<?php

use \yii\widgets\ListView;

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'=>'_categorias',
    'itemOptions'=>['class' => 'card'],
    'options'=> ['class' => 'card-colums'],
    'layout' => "{items}"
]);

